# -*- coding: utf-8 -*-
import db

# class Package():
#     def __init__(
#             self,
#             product_name,
#             tenant_id,
#             created_at,
#             resource_ref,
#             resource_name,
#             id_transaction,
#             id_admin):
#         self.product_name = product_name
#         self.tenant_id = tenant_id
#         self.created_at = created_at
#         self.resource_ref = resource_ref
#         self.resource_name = resource_name
#         self.id_transaction = id_transaction
#         self.id_admin = id_admin

# class Storage():
#     def __init__(
#             self,
#             product_name,
#             tenant_id,
#             created_at,
#             resource_ref,
#             resource_name,
#             id_transaction,
#             id_admin):
#         self.product_name = product_name
#         self.tenant_id = tenant_id
#         self.created_at = created_at
#         self.resource_ref = resource_ref
#         self.resource_name = resource_name
#         self.id_transaction = id_transaction
#         self.id_admin = id_admin

# class Addon():
#     def __init__(
#             self,
#             product_name,
#             tenant_id,
#             created_at,
#             resource_ref,
#             resource_name,
#             id_transaction,
#             id_admin):
#         self.product_name = product_name
#         self.tenant_id = tenant_id
#         self.created_at = created_at
#         self.resource_ref = resource_ref
#         self.resource_name = resource_name
#         self.id_transaction = id_transaction
#         self.id_admin = id_admin

class Account():
    def __init__(
            self,
            username,
            email,
            parent_user,
            role_id,
            hotline_permit,
            payment,
            api_key,
            pbx,
            c2c,
            tenant_id):
        self.username = username
        self.email = email
        self.parent_user = parent_user
        self.role_id = role_id
        self.hotline_permit = hotline_permit
        self.payment = payment
        self.api_key = api_key
        self.pbx = pbx
        self.c2c = c2c
        self.tenant_id = tenant_id

class Hotline():
    def __init__(
            self,
            hotline_number,
            type,
            quantity,
            audio,
            audio_time,
            server_ip,
            service_provider_ip,
            provider,
            status,
            id,
            sound,
            owner,
            id_package,
            id_addon,
            create_date,
            bucketid):
        self.hotline_number = hotline_number
        self.type = type
        self.audio = audio
        self.audio_time = audio_time
        self.server_ip = server_ip
        self.service_provider_ip = service_provider_ip
        self.provider = provider
        self.status = status
        self.id = id
        self.sound = sound
        self.owner = owner
        self.id_package = id_package
        self.id_addon = id_addon
        self.create_date = create_date
        self.bucketid = bucketid

class Package():
    def __init__(
            self,
            name_model,
            id,
            name,
            price,
            feature,
            num_ext,
            storage_call,
            unit):
        self.name_model = name_model
        self.id = id
        self.name = name
        self.price = price
        self.feature = feature
        self.num_ext = num_ext
        self.storage_call = storage_call
        self.unit = unit

class Addon():
    def __init__(
            self,
            name_model,
            id,
            name,
            price,
            desc,
            kind,
            value,
            unit):
        self.name_model = name_model
        self.id = id
        self.name = name
        self.price = price
        self.desc = desc
        self.kind = kind
        self.value = value
        self.unit = unit

class Transation():
    def __init__(
            self,
            id,
            tenant_id,
            resource_name,
            resource_ref,
            event_type,
            products_name,
            id_admin):
        self.name_model = name_model
        self.id = id
        self.name = name
        self.price = price
        self.feature = feature
        self.num_ext = num_ext
        self.storage_call = storage_call
        self.unit = unit