# -*- coding: utf-8 -*-
import config
import pika
import sys
import json


def send_task(transation):
    # RABBITMQ
    # credentials = pika.PlainCredentials(settings.user_rabbit, settings.pass_rabbit)

    # Push to RABBITMQ
    # connection = pika.BlockingConnection(
    # pika.ConnectionParameters(settings.IP_MASTER_RABBIT, 5672, '/',
    # credentials))
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=config.HOST))
    channel = connection.channel()
    # channel.queue_declare(queue=ip)
    # Declare the queue
    channel.queue_declare(queue=config.QUEUE, exclusive=False, auto_delete=False)
    # Enabled delivery confirmations
    channel.confirm_delivery()

    message = json.dumps(transation)
    # print(message)
    status = channel.basic_publish(exchange='',
                          routing_key=config.ROUTING_KEY,
                          body=message,
                          properties=pika.BasicProperties(
                              delivery_mode=2,  # make message persistent
                          ), mandatory=True)
    print(status)
    print(" [x] Sent %r" % message)
    # Cancel the consumer and return any pending messages
    requeued_messages = channel.cancel()
    print('Requeued %i messages' % requeued_messages)

    channel.close()
    connection.close()
