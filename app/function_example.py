def get_document(table, query,
                 order=None,
                 distinct=None,
                 page=None,
                 limit=None,
                 incre=-1):
    if page:
        if not limit:
            limit = 20
        page = int(page)
        limit = int(limit)
        if order:
            res = DATABASE[table].find(query)\
                                 .sort(order, incre)\
                                 .skip((page-1)*limit)\
                                 .limit(limit)
        else:
            res = DATABASE[table].find(query)\
                                 .skip((page-1)*limit)\
                                 .limit(limit)
    else:
        if order:
            res = DATABASE[table].find(query).sort(order, incre)
        else:
            res = DATABASE[table].find(query)
    if distinct:
        res = res.distinct(distinct)
        res = filter(lambda r: r != "", res)
    if res:
        message = {"data": list(res)}
    else:
        message = {"data": []}
    return message

def insert_document(table, query, field_unique=None):
    if field_unique:
        DATABASE[table].create_index(field_unique, unique=True)
    try:
        DATABASE[table].insert(query)
        message = {"status": True, "message": "Insert success"}
        if "id" in query:
            message.update({"id": query["id"]})
    except:
        message = {"status": False, "message": "{} is exists.".format(field_unique)}
    return message