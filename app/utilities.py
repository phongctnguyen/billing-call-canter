# -*- coding: utf-8 -*-
import models
import db
from flask_restful import reqparse, abort, fields, marshal_with, Api, Resource, reqparse
from flask import request, g
from functools import wraps
import dateutil.parser

"""
Các hàm tiện ích
"""

# Kiểm tra thông tin account được gửi đến
# def check_account_request(username, email):
#     if db.find_one(
#                 "tbl_account", {"username": username}) is not None:
#         package_account = db.find_one(
#                 "tbl_account", {"username": username})
#         return package_account
#     elif email and db.find_one("tbl_account", {"email": email}) is not None:
#         package_account = db.find_one(
#             "tbl_account", {"email": email})
#         return package_account
#     else:
#         abort(400, status="false", message="Fail")
#         return {}, 400

# Kiểm tra thông tin hotline được gửi đến


def check_hotline_request(hotline_number):
    if db.find_one(
            "tbl_hotline", {"hotline_number": hotline_number}) is not None:
        package_hotline = db.find_one(
            "tbl_hotline", {
                "hotline_number": hotline_number})
        return package_hotline
    else:
        abort(400, message="Number {} doesn't exist".format(hotline_number))
        return {status: "failed"}, 400

# Kiểm tra thông tin gói cước được gửi đến


def check_package(name):
    if db.find_one(
            "tbl_package", {"name": name}) is not None:
        package_info = db.find_one(
            "tbl_package", {"name": name})
        return package_info
    else:
        abort(
            400,
            message="Package {} doesn't exist".format(name),
            status="failed")
        return {status: "failed"}, 400


def check_addon(name):
    if db.find_one(
            "tbl_addon", {"name": name}) is not None:
        addon_info = db.find_one(
            "tbl_addon", {"name": name})
        return addon_info
    else:
        abort(400, message="Addon {} doesn't exist".format(name))
        return {status: "failed"}, 400


def check_storage(name):
    if db.find_one(
            "tbl_addon", {"name": name}) is not None:
        storage_info = db.find_one(
            "tbl_addon", {"name": name})
        return storage_info
    else:
        abort(400, message="Storage {} doesn't exist".format(name))
        return {status: "failed"}, 400

# Kiểm tra thông tin email được gửi đến


def check_email_request(email):
    if db.find_one("tbl_account", {"email": email}) is not None:
        package_account = db.find_one(
            "tbl_account", {"email": email})
        return package_account
    else:
        abort(400, message="Email {} doesn't exist".format(email))
        return {status: "failed"}, 400


def get_user_id(token):
    try:
        return token
    except Exception as e:
        return None


def authenticate(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # access_token = request.headers.get('Authorization')
        access_token = 'test'
        # # print(access_token)
        # g.status = False
        if not access_token:
            return "Require Access Token"
        # # if access_token:
        # #     user_id = get_user_id(access_token)
        #     # print(user_id)
        # #     if user_id:
        # #         g.user_id = str(user_id)
        # #         g.access_token = access_token
        # #         g.user = get_user_info(user_id)
        # #         g.status = True
        # # if not g.status:
        # #     return "Wrong Access Token", 400
        if access_token == 'test':
            pass
        else:
            return "Wrong Access Token", 400
        return f(*args, **kwargs)
    return decorated_function


def check_id(doc, collection, id):
    if doc != db.find_one(collection, {"id": id}):
        abort(400, message="Wrong ID")
        return {status: "failed"}, 400


def check_date(date):
    try:
        return dateutil.parser.parse(date)
    except ValueError:
        abort(400, message="Wrong datetime")
        return {status: "failed"}, 400

def check_tenant_id(col):
    if 'tenant_id' in col.keys():
        return col['tenant_id']
    else:
        return None