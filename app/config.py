# -*- coding: utf-8 -*-
import os

DEBUG = True
ENV = 'development'
KEY = 'secret'
DB_HOST = 'localhost'
DB_PORT = 27017
SESSION_COOKIE_DOMAIN = None
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_NAME = 'session'
SESSION_COOKIE_PATH = None
SESSION_COOKIE_SAMESITE = None
SESSION_COOKIE_SECURE =  False
SESSION_REFRESH_EACH_REQUEST = True
HOST = 'localhost'
QUEUE = 'queue'
ROUTING_KEY = 'queue'

