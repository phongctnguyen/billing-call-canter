# -*- coding: utf-8 -*-
from flask import Flask
from flask import Flask, render_template, send_from_directory, request, redirect, session, flash, url_for, jsonify
import models, resources
import sys
from flask_restful import reqparse, abort, Api, Resource

app = Flask(__name__)
api = Api(app)

api.add_resource(
    resources.Package,
    '/api/v1/billing/package',
    endpoint='packages')
api.add_resource(
    resources.Package,
    '/api/v1/billing/package/<string:id>',
    endpoint='package')
api.add_resource(resources.Addon, '/api/v1/billing/addon', endpoint='addons')
api.add_resource(
    resources.Addon,
    '/api/v1/billing/addon/<string:id>',
    endpoint='addon')
api.add_resource(resources.Transaction, '/api/v1/billing/transaction', endpoint='transactions')
api.add_resource(
    resources.Transaction,
    '/api/v1/billing/transaction/<string:id>',
    endpoint='transaction')


if __name__ == '__main__':
  try:
    port = int(sys.argv[1])
  except (TypeError, IndexError):
    port = 80
  app.run(debug=True, port=port)
