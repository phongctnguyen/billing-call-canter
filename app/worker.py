# -*- coding: utf-8 -*-
from app import config
import pika
import time

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=config.HOST))
channel = connection.channel()

channel.queue_declare(queue=config.QUEUE)
print(' [*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    time.sleep(body.count(b'.'))
    print(" [x] Done")
    ch.basic_ack(delivery_tag=method.delivery_tag)


# Tells RabbitMQ not to give more than one message to a worker at a time.
channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=config.QUEUE, on_message_callback=callback)

channel.start_consuming()
