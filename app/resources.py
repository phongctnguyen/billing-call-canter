# -*- coding: utf-8 -*-
import db
import models
import utilities
import new_task
import config
from flask import Flask, render_template, send_from_directory, request, redirect, session, flash, url_for, jsonify, g
from flask_restful import reqparse, abort, fields, marshal_with, Api, Resource, reqparse
from datetime import datetime
import dateutil.parser
# from functools import wraps
import functools
import uuid
import argparse
import json
from bson import ObjectId

# Input
parser = reqparse.RequestParser()
parser.add_argument('username')
parser.add_argument('email', required=True, help="Email cannot be blank!")
parser.add_argument(
    'resource_name',
    required=True,
    help="Hotline cannot be blank!")
parser.add_argument(
    'product_name',
    required=True,
    help="Product name cannot be blank!")
parser.add_argument(
    'event_type',
    required=True,
    help="Event type cannot be blank!")
parser.add_argument(
    'id_admin',
    required=True,
    help="ID Admin cannot be blank!")
# parser.add_argument('addon_name', required=True, help="Addon name cannot be blank!")

# Input get info addon
parser_addon = reqparse.RequestParser()
parser_addon.add_argument('email')
parser_addon.add_argument('resource_name')

# Input get info user
parser_user_package = reqparse.RequestParser()
parser_user_package.add_argument('email')
parser_user_package.add_argument('resource_name')

# Input get info transaction
parser_log = reqparse.RequestParser()
parser_log.add_argument('email')
parser_log.add_argument('resource_name')
parser_log.add_argument('from_date',
                        required=True,
                        help="From date cannot be blank!")
parser_log.add_argument('to_date',
                        required=True,
                        help="To date cannot be blank!")


# Input delete addon
parser_delete_addon = reqparse.RequestParser()
parser_delete_addon.add_argument(
    'email',
    required=True,
    help="Email cannot be blank!")
parser_delete_addon.add_argument(
    'resource_name',
    required=True,
    help="Hotline cannot be blank!")
parser_delete_addon.add_argument(
    'resource_ref',
    required=True,
    help="Resource ref cannot be blank!")
parser_delete_addon.add_argument(
    'event_type',
    required=True,
    help="Event type cannot be blank!")


class Package(Resource):
    """
    Tiếp nhận và xử lý các request liên quan đến sửa, cập nhật, xóa gói sản phẩm
    tương ứng với từng user
    """
    @utilities.authenticate
    # @marshal_with(packages)
    def get(self):
        parsed_args = parser_user_package.parse_args()
        hotlines = []

        # Input đầu vào là email, thông tin trả ra là danh sách thông tin của
        # các hotline liên quan
        if (parsed_args['email']):
            package_account = utilities.check_email_request(
                parsed_args['email'])

            for hotline in db.find_all(
                'tbl_hotline', {
                    'owner': str(package_account['_id'])}):
                hotline.pop('_id', None)
                hotlines.append(hotline)
            return json.dumps({'status':'success', 'payload': hotlines}), 200

        # Input đầu vào là hotline, trả ra thông tin của hotline đó
        elif (parsed_args['resource_name']):
            hotline = utilities.check_hotline_request(
                parsed_args['resource_name'])
            hotline.pop('_id', None)
            return json.dumps({'status':'success', 'payload': hotline}), 200

        else:
            abort(400, message="Wrong input")
            return {status: "failed"}, 400

    @utilities.authenticate
    # @marshal_with(packages)
    def post(self):
        parsed_args = parser.parse_args()
        dict_package = {}
        dict_hotline = {}

        package_account = utilities.check_email_request(parsed_args['email'])
        hotline = utilities.check_hotline_request(parsed_args['resource_name'])
        package_name = utilities.check_package(parsed_args['product_name'])

        tenant_id = utilities.check_tenant_id(package_account)
        resource_name = hotline['hotline_number']
        product_name = package_name['name']

        # Kiểm tra trường "resource_ref", nếu chưa có thì thêm vào db
        if 'resource_ref' in hotline.keys():
            resource_ref = hotline['resource_ref']

        else:
            dict_hotline['resource_ref'] = str(uuid.uuid4())
            resource_ref = dict_hotline['resource_ref']

        dict_hotline['id_package'] = package_name['id'] if 'id' in package_name.keys(
        ) else None
        dict_hotline['package_info'] = {
            "name": package_name['name'],
            "status": "active"
        }

        db.update_document(
            "tbl_hotline", {
                "hotline_number": parsed_args['resource_name']}, dict_hotline)

        # Output
        dict_package['product_name'] = product_name
        dict_package['tenant_id'] = tenant_id
        dict_package['created_at'] = str(datetime.now().isoformat())
        dict_package['resource_name'] = resource_name
        dict_package['resource_ref'] = resource_ref

        # Đẩy vào Rabbit MQ
        new_task.send_task(dict_package)

        # Lưu vào bảng tbl_log_transaction
        dict_log = dict_package.copy()
        dict_log.pop('status', None)
        dict_log['id'] = str(uuid.uuid4())
        dict_log['event_type'] = parsed_args['event_type']
        db.add_document("tbl_log_transaction", dict_log)

        return json.dumps({'status': 'success', 'payload': dict_package}), 201

    # Cập nhật gói
    @utilities.authenticate
    # @marshal_with(packages)
    def put(self, id):

        parsed_args = parser.parse_args()
        dict_update = {}
        dict_package = {}
        dict_log = {}

        package_account = utilities.check_email_request(parsed_args['email'])
        hotline = utilities.check_hotline_request(parsed_args['resource_name'])
        package_name = utilities.check_package(parsed_args['product_name'])

        # Kiểm tra id được truyền vào
        utilities.check_id(package_name, "tbl_package", id)

        tenant_id = utilities.check_tenant_id(package_account)

        # Thêm trường package_info vào bảng tbl_hotline
        dict_update['package_info'] = {
            "name": package_name['name'],
            "status": "active"
        }
        dict_update['id_package'] = package_name['id'] if 'id' in package_name.keys(
        ) else None

        # Cập nhật thông tin vào bảng tbl_hotline
        db.update_document(
            "tbl_hotline", {
                "hotline_number": parsed_args['resource_name']}, dict_update)

        # Output
        dict_package['product_name'] = dict_update['package_info']['name']
        dict_package['tenant_id'] = tenant_id
        dict_package['created_at'] = str(datetime.now().isoformat())
        dict_package['resource_name'] = hotline['hotline_number']
        dict_package['resource_ref'] = hotline['resource_ref']

        # Đẩy vào Rabbit MQ
        new_task.send_task(dict_package)

        # Lưu vào bảng tbl_log_transaction
        dict_log = dict_package.copy()
        dict_log.pop('status', None)
        dict_log['id'] = str(uuid.uuid4())
        dict_log['event_type'] = parsed_args['event_type']
        db.add_document("tbl_log_transaction", dict_log)

        return json.dumps({'status': 'success', 'payload': dict_package}), 200

    # Xóa gói
    @utilities.authenticate
    def delete(self, id):
        parsed_args = parser.parse_args()
        dict_update = {}
        dict_package = {}
        dict_log = {}

        package_account = utilities.check_email_request(parsed_args['email'])
        hotline = utilities.check_hotline_request(parsed_args['resource_name'])
        package_name = utilities.check_package(parsed_args['product_name'])

        # Kiểm tra id được truyền vào
        utilities.check_id(package_name, "tbl_package", id)

        tenant_id = utilities.check_tenant_id(package_account)

        # Chuyển trạng thái trường package_info sang inactive

        if 'id_package' in hotline.keys() and hotline['id_package'] == id:
            dict_update['package_info'] = {
                "name": package_name['name'],
                "status": "inactive"
            }
        else:
            abort(400, message="Id does not match with an existing package")
            return {}, 400

        # Cập nhật thông tin vào bảng tbl_hotline
        db.update_document(
            "tbl_hotline", {
                "hotline_number": parsed_args['resource_name']}, dict_update)

        # Output
        dict_package['tenant_id'] = tenant_id
        dict_package['created_at'] = str(datetime.now().isoformat())
        dict_package['resource_name'] = hotline['hotline_number']

        # Đẩy vào Rabbit MQ
        new_task.send_task(dict_package)

        # Lưu vào bảng tbl_log_transaction
        dict_log = dict_package.copy()
        dict_log.pop('status', None)
        dict_log['id'] = str(uuid.uuid4())
        dict_log['event_type'] = parsed_args['event_type']
        dict_log['product_name'] = dict_update['package_info']['name']
        dict_log['resource_ref'] = hotline['resource_ref']
        db.add_document("tbl_log_transaction", dict_log)

        return json.dumps({'status': 'success', 'payload': dict_package}), 204


class Addon(Resource):
    """
    Tiếp nhận và xử lý các request liên quan đến sửa, cập nhật, xóa storage
    tương ứng với từng user
    """

    @utilities.authenticate
    def get(self):
        parsed_args = parser_addon.parse_args()
        dict_addon = []

        if (parsed_args['email']):
            package_account = utilities.check_email_request(
                parsed_args['email'])
            hotlines = db.find_all(
                'tbl_hotline', {
                    'owner': str(package_account['_id'])})
            print(hotlines)
            for hotline in hotlines:
                if 'addon_storage' in hotline.keys():
                    for hotline_addon in hotline['addon_storage']:
                        dict_addon.append(hotline_addon)
            print(json.dumps(dict_addon))
            return json.dumps({'status':'success', 'payload': dict_addon}), 200


        # Input đầu vào là hotline, trả ra thông tin của hotline đó
        elif (parsed_args['resource_name']):
            hotlines = utilities.check_hotline_request(
                parsed_args['resource_name'])
            if 'addon_storage' in hotlines.keys():
                return json.dumps({'status': 'success', 'payload': hotlines['addon_storage']})
            else:
                abort(400, message="addon_storage doesn't exist")
                return {}, 400

        else:
            abort(400, message="Wrong input")
            return {status: "failed"}, 400

    # @login_required
    @utilities.authenticate
    # @marshal_with(addons)
    def post(self):
        parsed_args = parser.parse_args()
        dict_addon = {}
        dict_log = {}
        dict_hotline = {}

        # Kiểm tra input
        package_account = utilities.check_email_request(parsed_args['email'])
        hotline = utilities.check_hotline_request(parsed_args['resource_name'])
        addon = utilities.check_addon(parsed_args['product_name'])

        if (addon['kind'] == 'storage'):
            addon_name = utilities.check_storage(parsed_args['product_name'])
        elif (addon['kind'] == 'addon'):
            addon_name = utilities.check_addon(parsed_args['product_name'])
        else:
            abort(400, message="Wrong addon name input")
            return {}, 400

        tenant_id = utilities.check_tenant_id(package_account)

        product_name = addon_name['name']
        resource_ref = str(uuid.uuid4())
        resource_name = hotline['hotline_number']

        if 'package_info' in hotline.keys():
            related_ref = hotline['resource_ref']

        else:
            abort(400, message="You must register package first")
            return {}, 400

        # Ouput
        dict_addon['product_name'] = product_name
        dict_addon['resource_ref'] = resource_ref
        dict_addon['tenant_id'] = tenant_id
        dict_addon['created_at'] = str(datetime.now().isoformat())
        dict_addon['related_ref'] = related_ref
        dict_addon['resource_name'] = resource_name

        # Thêm trường addon_storage vào bảng tbl_hotline
        if 'addon_storage' not in hotline.keys(
        ) and addon['kind'] == 'storage':
            # Cập nhật các addon vào trường addon_storage
            dict_hotline['addon_storage'] = [{
                "product_name": dict_addon['product_name'],
                "resource_ref": dict_addon['resource_ref'],
                "created_at": dict_addon['created_at']
            }]
            
            # Đẩy sang RABBITMQ
            new_task.send_task(dict_addon)

            # Cập nhật thông tin vào bảng tbl_hotline
            db.update_document(
                "tbl_hotline", {
                    "hotline_number": parsed_args['resource_name']}, dict_hotline)
        elif (addon['kind'] == 'storage'):
            dict_hotline['addon_storage'] = hotline['addon_storage']
            dict_hotline['addon_storage'].append(
                {
                    "product_name": dict_addon['product_name'],
                    "resource_ref": dict_addon['resource_ref'],
                    "created_at": dict_addon['created_at']
                }
            )

            # Đẩy sang RABBITMQ
            new_task.send_task(dict_addon)

            # Cập nhật thông tin vào bảng tbl_hotline
            db.update_document(
                "tbl_hotline", {
                    "hotline_number": parsed_args['resource_name']}, dict_hotline)

        # Lưu vào bảng tbl_log_transaction
        dict_log = dict_addon.copy()
        dict_log.pop('status', None)
        dict_log['id'] = str(uuid.uuid4())
        dict_log['event_type'] = parsed_args['event_type']
        db.add_document("tbl_log_transaction", dict_log)

        return json.dumps({'status': 'success', 'payload': dict_addon}), 201

    @utilities.authenticate
    def delete(self, id):
        parsed_args = parser_delete_addon.parse_args()
        dict_addon = {}
        dict_log = {}
        dict_hotline = {}
        count = 0

        package_account = utilities.check_email_request(parsed_args['email'])
        hotline = utilities.check_hotline_request(parsed_args['resource_name'])

        # Kiểm tra id được truyền vào
        if hotline != db.find_one('tbl_hotline', {"id": id}):
            abort(400, message="Wrong ID")
            return {status: "failed"}, 400

        tenant_id = utilities.check_tenant_id(package_account)

        if 'addon_storage' not in hotline.keys() or len(
                hotline['addon_storage']) == 0:
            abort(400, status="failed", message="Addon storage doesn't exist")
            return {}, 400
        else:
            dict_hotline['addon_storage'] = hotline['addon_storage']
            for add_on in dict_hotline['addon_storage']:
                print(add_on)
                if add_on['resource_ref'] == parsed_args['resource_ref']:  
                    count += 1           
                    dict_hotline['addon_storage'].remove(add_on)
                    break
            if count == 0:
                abort(
                    400,
                    status="failed",
                    message="Resource ref doesn't exist")
                return {}, 400
        
         # Output
        dict_addon['tenant_id'] = tenant_id
        dict_addon['created_at'] = str(datetime.now().isoformat())
        dict_addon['resource_name'] = hotline['hotline_number']

        # Đẩy vào Rabbit MQ
        new_task.send_task(dict_addon)

        # Cập nhật thông tin vào bảng tbl_hotline
        db.update_document(
            "tbl_hotline", {
                "hotline_number": parsed_args['resource_name']}, dict_hotline)

        # Lưu vào bảng tbl_log_transaction
        dict_log = dict_addon.copy()
        dict_log.pop('status', None)
        dict_log['id'] = str(uuid.uuid4())
        dict_log['event_type'] = parsed_args['event_type']
        dict_log['related_ref'] = hotline['resource_ref']
        dict_log['resource_ref'] = parsed_args['resource_ref']
        db.add_document("tbl_log_transaction", dict_log)

        return json.dumps({'status':'success', 'payload': dict_addon}), 204


class Transaction(Resource):
    """
    Tiếp nhận và xử lý các request liên quan đến sửa, cập nhật, xóa gói sản phẩm
    tương ứng với từng user
    """
    @utilities.authenticate
    # @marshal_with(packages)
    def get(self):
        parsed_args = parser_log.parse_args()
        hotline_numbers = []
        log_transactions = []
        from_date = utilities.check_date(parsed_args['from_date'])
        to_date = utilities.check_date(parsed_args['to_date'])

        if (parsed_args['email']):
            package_account = utilities.check_email_request(
                parsed_args['email'])
            # Lấy ra tất cả hotline number tương ứng với account đó
            for hotline_number in db.find_all(
                'tbl_hotline', {
                    'owner': str(package_account['_id'])}):
                hotline_numbers.append(hotline_number['hotline_number'])

            for hotline_number in hotline_numbers:
                log_dict = {}
                log_dict['phone_number'] = hotline_number
                log_dict['history_transaction'] = []
                for log_transaction in db.find_all(
                    'tbl_log_transaction', {
                        'resource_name': hotline_number}, None, None, 1, 5):
                    # print(log_transaction)
                    # Hiển thị log trong một khoảng thời gian
                    if (
                        from_date < dateutil.parser.parse(
                            log_transaction['created_at']) and dateutil.parser.parse(
                            log_transaction['created_at']) < to_date):
                        log_transaction.pop('_id', None)
                        log_dict['history_transaction'].append(
                            (log_transaction))
                log_transactions.append(log_dict)
            print(log_transactions)
            return json.dumps({'status': 'success', 'payload': log_transactions})

        elif (parsed_args['resource_name']):
            hotline = utilities.check_hotline_request(
                parsed_args['resource_name'])

            log_dict = {}
            log_dict['phone_number'] = hotline['hotline_number']
            log_dict['history_transaction'] = []
            for log_transaction in db.find_all(
                'tbl_log_transaction', {
                    'resource_name': hotline['hotline_number']}, None, None, 1, 5):
                # Hiển thị log trong một khoảng thời gian
                if (
                    from_date < dateutil.parser.parse(
                        log_transaction['created_at']) and dateutil.parser.parse(
                        log_transaction['created_at']) < to_date):
                    log_transaction.pop('_id', None)
                    log_dict['history_transaction'].append((log_transaction))
            log_transactions.append(log_dict)
            print(log_transactions)
            return json.dumps({'status': 'success', 'payload': log_transactions})

        else:
            abort(400, message="Wrong input")
            return {status: "failed"}, 400
